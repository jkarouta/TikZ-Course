\part{Prerequisites}
\label{sec:prereq}

%=========================================
%========= CHAPTER: INTRODUCTION ========= 
%=========================================

\chapter{Introduction}
\label{sec:I_intro}
As brief as I can be, Ti\textit{k}Z is a package that enables you to create figures, diagrams, graphs and other graphics within a \LaTeX{} or \TeX{} environment. It is a higher level ``programming'' way expanding on the PGF package. Therefore, you will often see those two being used together or interchangeably. Sometimes it makes more sense to use the one over the other, but we will mainly discuss concepts applicable to the general concepts of creating a graphic. This means we will use both the PGF and the Ti\textit{k}Z packages at some point in this course.

\begin{figure}[ht] %\label{fig:exp_tikz} fig:tikzpicture & fig:exp_triangles
\centering

\begin{subfigure}[t]{0.45\textwidth}
    \resizebox {\textwidth} {!} {

        \begin{tikzpicture}[node distance=5mm and 5mm]
             \node[exp, start_node]     (node1)    {Start here};
             \node[exp, below=of node1] (node2)    {Then do this};
             \node[exp, below=of node2] (node3)    {Lastly do this};
             \node[exp, below=of node3] (node4)    {Finished!};
             
             \path  (node1) edge[->] (node2)
                    (node2) edge[->] (node3)
                    (node3) edge[->] (node4);
             
        \end{tikzpicture}
    }
    \caption{A simple Ti\textit{k}Z diagram}
    \label{fig:exp_diagram}
\end{subfigure}
~
\begin{subfigure}[t]{0.45\textwidth}
    \resizebox {\textwidth} {!} {
        \newcounter{density}
        \input{fig/example}
    }
    \caption{Geometrical figure, from Section~\ref{sec:testcode}}
    \label{fig:exp_triangles}
\end{subfigure}


\caption{Examples of figures produced with Ti\textit{k}Z}
\label{fig:exp_tikz}

\end{figure}

The course will go through some of the basics of making images, and embedding them into your \LaTeX{} document. We will start by creating simple diagrams like in Figure~\ref{fig:exp_diagram}. We will discuss both relative and absolute positioning and ways to apply styles and effects. Later we will also discuss ways to really ``program'' pictures using iterative processes, used for images like Figure~\ref{fig:exp_triangles}.



\section{Why use Ti\textit{k}Z?}
Although I expect part of this explanation to be common sense, it is important to understand.

When saving pictures digitally, all information is typically stored as an image in bitmap format (common filetypes are jpg and png). Such formats store information per pixel, which for large images can become quite substantial. For high quality pictures we are talking about 2-5 Mb of space. Fill a document with 10 of those, and it becomes a hassle to share. Moreover, when zooming in, you will find images can become quite foggy due to anti-aliasing and/or low quality images. You can check the difference between Ti\textit{k}Z and bitmap in Figure~\ref{fig:exp_tikz_bitmap}. Zoom in, and see the difference. The png is a saved screenshot in the same resolution.


\begin{figure}[ht] %\label{fig:exp_tikz_bitmap} fig:exp_triangles_tikz & fig:exp_triangles_bitmap
\centering


\begin{subfigure}[t]{0.45\textwidth}
    \resizebox {\textwidth} {!} {
        \input{fig/example}
    }
    \caption{Geometrical figure, created in Ti\textit{k}Z}
    \label{fig:exp_triangles_tikz}
\end{subfigure}
~
\begin{subfigure}[t]{0.45\textwidth}
    \resizebox {\textwidth} {!} {
        \includegraphics[]{fig/triangles_bitmap.png}
    }
    \caption{Geometrical figure in bitmap (png)}
    \label{fig:exp_triangles_bitmap}
\end{subfigure}


\caption{Examples of figures produced with Ti\textit{k}Z}
\label{fig:exp_tikz_bitmap}

\end{figure}

Luckily this is where vector graphics come in! You might be familiar with software like Adobe Illustrator or Inkscape, or file formats like .ai, .eps, and .svg. These are all formats that don't store pixel information per se, but generate the file based on text-based commands. A one-coloured circle spanning 1500x1500 pixels should not contain $A=\pi r^2=\pi750^2\approx1.77\times10^6 \textrm{MP}$ of data, but instead would consist of a location of its mid-point, radius, border thickness and colors. Especially for larger pictures the gains (size-wise) are big. Also the quality stays good, because the image is rendered in real-time, and zooming will not show pixels.

Another huge benefit, is that the source of your image is right there along with your text. This means that you can quickly edit your image; you can embed parts of other images, references and other \LaTeX{} commands right into your image; you can even define the text and fonts to match the rest of your document, and use consistent shapes and sizes for all figures.



\section{When to use Ti\textit{k}Z?}
Wow, can't we convert all pictures to vectors? The answer is ``sounds good, doesn't work''. There are converters out there, but converting is often synonymous to loosing information. This is especially true for bitmap $\longleftrightarrow$ vector conversions. I won't go into details but in general the following decision diagram is good to follow:

\begin{figure}[ht] % \label{fig:when}
\centering
\resizebox {\textwidth} {!} {
    \input{fig/when}
}

\caption{Decision tree when to use Ti\textit{k}Z}
\label{fig:when}

\end{figure}

Besides these rules of thumb, think about how much time you want to put into it. Sometimes a quick sketch can be handier. Making diagrams using ``WYSIWYG'' editors can sometimes save a lot of time, and if you can export those to pdf or eps format, you will often still have the benefits of small file size. Remember that changing something later on can turn out to be tricky, and you would lack consistency across figures.

%=========================================
%========= CHAPTER: INSTALLATION ========= 
%=========================================

\chapter{Installation}
Before the course begins, please make sure you have a working \LaTeX{} distribution at your disposal. Either local or cloud application is good for this course, and I recommend the following.

\section{Working Locally}
Firstly you will need an editor. I have always used \TeX{}studio, however, you can also use Visual Studio Code, with the ``LaTeX Workshop'' extension. 

In both cases you need a compiler, and I recommend using \href{https://miktex.org/download}{MiK\TeX}. Just follow the installation guidelines and you should be good to go! Whenever compiling a document that uses a new package, MiK\TeX{} will ask you for permission to install the missing ones or do it automatically (depending on your choice during install).


\section{Working Online}
If you want to work in the cloud, or just don't want to install software, I recommend using \href{https://www.overleaf.com/}{Overleaf}. Make an account and you should be good to go! Here you don't ever need to worry about installing packages, or even to loose your files. You can access them from anywhere. Just be aware of NDA sensitive material.

\newpage
\section{Check}
\label{sec:testcode}
Check that everything runs correctly by compiling the following code:

\begin{itemize}
    \item[\color{red!60!black}\textbf{NOTE $\blacktriangleright$}] Remove the space between \textbackslash{} and \texttt{documentclass}
\end{itemize}

\begin{verbatim}
\ documentclass{standalone}
\usepackage[usenames,dvipsnames,pdftex]{xcolor}
\usepackage{tikz,ifthen}

\begin{document}
\newcounter{density}
\setcounter{density}{20}
\begin{tikzpicture}
    \def\couleur{MidnightBlue}
    \path[coordinate] (0,0)  coordinate(A)
                ++( 60:12cm) coordinate(B)
                ++(-60:12cm) coordinate(C);
    \draw[fill=\couleur!\thedensity] (A) -- (B) -- (C) -- cycle;
    \foreach \x in {1,...,15}{%
        \pgfmathsetcounter{density}{\thedensity+10}
        \setcounter{density}{\thedensity}
        \path[coordinate] coordinate(X) at (A){};
        \path[coordinate] (A) -- (B) coordinate[pos=.15](A)
                            -- (C) coordinate[pos=.15](B)
                            -- (X) coordinate[pos=.15](C);
        \draw[fill=\couleur!\thedensity] (A)--(B)--(C)--cycle;
    }
\end{tikzpicture}
\end{document}
% Name : RotateTriangle
% Encoding : utf8
% Engine : pdflatex
% Author: Alain Matthes
\end{verbatim}

This should result in a document containing only Figure~\ref{fig:exp_triangles}.


%=========================================
%========= CHAPTER: USEFUL LINKS ========= 
%=========================================

\chapter{Useful Links}

Below list is a reference of useful links. Of course, this is not a full list, and Google is also there for you.

The best advice I can give is to consult the \textbf{Ti\textit{k}Z and PGF manual} at all times. It's an 880 page document with all the information you need. Of course I don't expect anyone to read it, but if you know what you are looking for, the table of contents and the find function will most certainly help you.

\begin{itemize}
    \item[\color{red!60!black}\textbf{WWW$\blacktriangleright$}] \href{http://www.texample.net/media/pgf/builds/pgfmanualCVS2012-11-04.pdf}{Ti\textit{k}Z (\& PGF) Manual} \\
    The best way to find answers quickly for specific questions
    
    \item[\color{red!60!black}\textbf{WWW$\blacktriangleright$}] \href{http://www.texample.net/tikz/examples/}{\TeX ample.net} \\
    Very handy to download examples you can edit yourself
    
    \item[\color{red!60!black}\textbf{WWW$\blacktriangleright$}] \href{http://texdoc.net/texmf-dist/doc/latex/pgfplots/pgfplots.pdf}{\textsc{pgfplots} Manual} \\
    The separate \textsc{pgfplots} manual, with everything about charts and different chart types.

    
    \item[\textbf{WWW$\blacktriangleright$}] \href{https://www.mathworks.com/matlabcentral/fileexchange/22022-matlab2tikz-matlab2tikz}{matlab2tikz} \\
    Very handy to convert MATLAB plots to Ti\textit{k}Z images.
    
    \item[\textbf{WWW$\blacktriangleright$}] \href{https://www.overleaf.com/learn/latex/LaTeX_Graphics_using_TikZ:_A_Tutorial_for_Beginners_(Part_1)\%E2\%80\%94Basic_Drawing}{A Ti\textit{k}Z Tutorial for Beginners (Overleaf)} \\
    Well written basic course... Luckily, this course shows a bit more!
\end{itemize}

